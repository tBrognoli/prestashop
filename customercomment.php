<?php

require_once 'models/CustomerCommentModel.php';

class customercomment extends Module
{
    // TODO Champs obligatoire pour acceder au module dans le back office
    public function __construct()
    {
        $this->name = 'customercomment';
        $this->author = 'IMIE';
        $this->displayName = $this->l('Customer comment');
        $this->description = $this->l('Hello World');
        $this->version = '1.0.0';

        $this->bootstrap = true;

        parent::__construct();


    }

    // TODO Fonction déclenchée quand l'action est lancé
    public function install()
    {
        return parent::install()
            && $this->registerHook('displayCustomerAccount')
            && $this->registerHook('displayHome')
            && $this->installDb();
    }

    public function uninstall()
    {
        Db::getInstance()->execute(
            'DROP TABLE IF EXISTS `'. _DB_PREFIX_ .'comment`');
        return parent::uninstall();
    }

    public function installDb()
    {
        return Db::getInstance()->execute('
		      CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'comment` (
			    `id_comment` INT PRIMARY KEY AUTO_INCREMENT,
			    `comment` varchar(1000),
			    `date_add` DATE,
			    `id_customer` INT UNIQUE,
			    `disabled` boolean not null default 0,
			    `rate` INT
		      ) ENGINE = ' . _MYSQL_ENGINE_ . ' CHARACTER SET utf8 COLLATE utf8_general_ci;');
    }

    // TODO Permet de lier le fichier css
    // TODO Penser à greffer dans apparence > position
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path . 'views/css/customercomment.css');
    }

    public function hookDisplayCustomerAccount($params)
    {
        return $this->display(__FILE__, 'my-account.tpl');
    }

    public function hookDisplayHome($params)
    {
        $result = Db::getInstance()->executeS(
            'SELECT a.*, b.firstname, b.lastname FROM '._DB_PREFIX_.'comment a JOIN '._DB_PREFIX_.'customer b ON a.id_customer = b.id_customer');
        $this->context->smarty->assign('comments', $result);
        return $this->display(__FILE__, 'commentHome.tpl');
    }

    public $tabs = array(
        array(
            'name' => 'Commentaire', // One name for all langs
            'class_name' => 'AdminCommentary',
            'visible' => true,
            'parent_class_name' => 'ShopParameters',
        ));
}