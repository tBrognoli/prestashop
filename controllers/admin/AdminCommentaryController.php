<?php
require_once _PS_MODULE_DIR_ . '/customercomment/models/CustomerCommentModel.php';
class AdminCommentaryController extends ModuleAdminController
{
    public function initContent()
    {
        parent::initContent();
    }

    /**
     * Instanciation de la classe
     * Définition des paramètres basiques obligatoires
     */
    public function __construct()
    {
        $this->bootstrap = true; //Gestion de l'affichage en mode bootstrap
        $this->table = CustomerCommentModel::$definition['table']; //Table de l'objet
        $this->identifier = CustomerCommentModel::$definition['primary']; //Clé primaire de l'objet
        $this->className = CustomerCommentModel::class; //Classe de l'objet

        //Appel de la fonction parente pour pouvoir utiliser la traduction ensuite
        parent::__construct();
        $this->_join .= ' JOIN '._DB_PREFIX_.'customer ON a.id_customer = '._DB_PREFIX_.'customer.id_customer';
        $this->_select = _DB_PREFIX_.'customer.firstname, '._DB_PREFIX_.'customer.lastname';
        //Liste des champs de l'objet à afficher dans la liste
        $this->fields_list = [
            'id_comment' => [ //nom du champ sql
                'title' => $this->module->l('ID'), //Titre
                'align' => 'center', // Alignement
                'class' => 'fixed-width-xs' //classe css de l'élément
            ],
            'date_add' => [
                'title' => $this->module->l('date_add'),
                'align' => 'left',
            ],
            'comment' => [
                'title' => $this->module->l('comment'),
                'align' => 'left',
            ],
            'id_customer' => [
                'title' => $this->module->l('id_customer'),
                'align' => 'left',
            ],
            'firstname' => [
                'title' => $this->module->l('firstname'),
                'align' => 'left',
            ],
            'lastname' => [
                'title' => $this->module->l('lastname'),
                'align' => 'left',
            ],
            'rate' => [
                'title' => $this->module->l('rate'),
                'align' => 'left',
            ],
            'disabled' => [
                'title' => $this->module->l('disabled'),
                'align' => 'left',
            ],
        ];

        //Ajout d'actions sur chaque ligne
        $this->addRowAction('edit');
        $this->addRowAction('delete');
    }

    public function renderForm()
    {
        //Définition du formulaire d'édition
        $this->fields_form = [
            //Entête
            'legend' => [
                'title' => $this->module->l('Edition du commentaire'),
                'icon' => 'icon-cog'
            ],
            //Champs
            'input' => [
                [
                    'type' => 'date',
                    'label' => $this->module->l('date_add'),
                    'name' => 'date_add',
                    'required' => true,
                ],
                [
                    'type' => 'textarea', //Type de champ
                    'label' => $this->module->l('Commentaire'), //Label
                    'name' => 'comment', //Nom
                    'class' => 'input fixed-width-lg', //classes css
                    'required' => true, //Requis ou non
                ],
                [
                    'type' => 'select',
                    'label' => $this->l('Note'),
                    'name' => 'rate',
                    'multiple' => false,
                    'options' => array(
                        'query' => array(
                            array('key' => '1', 'name' => '1'),
                            array('key' => '2', 'name' => '2'),
                            array('key' => '3', 'name' => '3'),
                            array('key' => '4', 'name' => '4'),
                            array('key' => '5', 'name' => '5'),
                        ),
                        'id' => 'key',
                        'name' => 'name'
                    ),
                    'hint' => $this->module->l('Rentre un nombre entre 1 et 5'),
                    'required' => true,
                ],
                [
                    'type' => 'text',
                    'label' => $this->module->l('Id Client'),
                    'name' => 'id_customer',
                    'class' => 'input fixed-width-sm ',
                    'disabled' => 'disabled',
                ],
                [
                    'type' => 'switch',
                    'label' => $this->l('Non visible'),
                    'name' => 'disabled',
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ],
            ],
            //Boutton de soumission
            'submit' => [
                'title' => $this->l('Save'), //On garde volontairement la traduction de l'admin par défaut
            ]
        ];
        return parent::renderForm();
    }


}