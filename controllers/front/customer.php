<?php

class customercommentCustomerModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $customer_id = $this->context->customer->id;
        $result = Db::getInstance()->getRow(
            'SELECT * FROM '._DB_PREFIX_.'comment WHERE id_customer = '.$customer_id);
        $ccModels = new CustomerCommentModel($result['id_comment']);

        if (Tools::isSubmit('submitSend'))
        {

            $comment = Tools::getValue('comment');
            $ccModels->comment = $comment;

            $rate = Tools::getValue('rate');
            $ccModels->rate = $rate;

            $ccModels->id_customer = $customer_id;

            $date = date('Y-m-d H:i:s');
            $ccModels->date_add = $date;

            if ($rate < 3) {
                $ccModels->disabled = 1;
            }
            else {
                $ccModels->disabled = 0;
            }
            $ccModels->save();

        }

        $this->context->smarty->assign('comment', $result['comment']);
        $this->context->smarty->assign('rate', $result['rate']);
        $this->setTemplate('module:customercomment/views/templates/front/template.tpl');
        parent::initContent();
    }
}