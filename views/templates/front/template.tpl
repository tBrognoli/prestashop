{extends file='page.tpl'}
{block name='page_content'}
    <form action="" method="post">
        <div>
            <label for="comment">Commentaire: </label>
            <input type="text" name="comment" value="{if isset($comment)}{$comment}{/if}" required>
        </div>
        <div>
            <label for="rate">Votre note: </label>
            <input type="number" min="0" max="5" value="{if isset($rate)}{$rate}{/if}" name="rate">
        </div>
        {if isset($date)}{$date}{/if}
        <div class="form-example">
            <input type="submit" name='submitSend' value="Send">
        </div>
    </form>
{/block}